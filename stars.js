function shape1(n) {
	let result = "";
	for (let i = 0; i < n; i++) {
		for (let j = 0; j < n; j++) {
			result = result + "* ";
		}
		result = result + "\n";
	}
	console.log(result);
	console.log();
	console.log();
}

//i   j 0 1 2 3
//0     * * * *
//1     *     *
//2     *     *
//3     * * * *
function shape2(n) {
	let result = "";
	for (let i = 0; i < n; i++) {
		if (i == 0 || i == n - 1) {
			for (let j1 = 0; j1 < n; j1++) {
				result = result + "* ";
			}
		} else {
			for (let j3 = 0; j3 < n; j3++) {
				if (j3 == 0 || j3 == n - 1) {
					result = result + "* ";
				} else {
					result = result + "  ";
				}
			}
		}

		result = result + "\n";
	}
	console.log(result);
	console.log();
	console.log();
}

// N=4
// ___*_
// __*_*_
// _*_*_*_
// *_*_*_*_
function shape3(n) {
	let result = "";
	for (let i = 0; i < n; i++) {
		for (let j = 0; j < n - 1 - i; j++) {
			result = result + " ";
		}

		for (let j = 0; j < i + 1; j++) {
			result = result + "* ";
		}

		result = result + "\n";
	}
	console.log(result);
	console.log();
	console.log();
}

shape1(15);
shape2(20);
shape3(30);
